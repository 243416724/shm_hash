#ifndef _hash_value_block_
#define _hash_value_block_

#include "stdint.h"
#include "simple_offset_list.h"
#include <vector>
#include "define.h"


//单元格状态
enum  HashValueCellStatus 
{
	//空闲，无效
	HVCS_IDLE,
	//已经释放的
	HVCS_RELEASED,
	//被分配准备使用,
	HVCS_PRE_ALLOCED,
	//被设置了值,
	HVCS_WRITED_VALUE,
	//操作成功
	HVCS_SETED_OK
};

struct HashBlockHead
{
	//块magicid
	char   magicID[16];// = { "_HashBlockHead_" };
	//版本号
	uint16_t  versionNo;
	//头大小
	uint16_t  blockHeadSize;
	//尾大小
	uint16_t  blockTailSize;
	//事务数据大小
	uint32_t  tranHeadSize;
	//值单元格个数
	uint32_t  cellNum;
	//单元格头大小
	uint32_t  cellHeadSize;
	//单元格大小
	uint32_t  cellSize;
	//单元格数据区大小
	uint32_t  cellDataSize;
	//單元格所有數據大小
	uint64_t  cellToatlSize;
	//块大小
	uint64_t  blockSize;
	//单元格起始位置偏移
	uint32_t  cellStartOffset;
	//单元格结束位置偏移
	uint64_t  cellEndOffset;
	//创建时间
	uint64_t  createTime;
	char      blockInitTag[16];
}__attribute__((__packed__));

struct HashValueCell
{
	volatile uint64_t next;
	s_offset_list_t   valHead;
	uint64_t ValueSeq;
	//头节点记录value总长，其余节点记录在节点的实际长度
	uint32_t lengthInCell;
	//单元格状态 HashValueCellStatus
	uint32_t  cellStatus : 4;
	uint32_t  notUserd : 28;//32
	uint8_t   bufData[0];
}__attribute__((__packed__));

enum  HashBlockTransactionStatus {
	//空闲  (到这里是完成事务)
	HBTC_IDLE,  //1
	//-------------------------------------------------
	//分配内存(事务起始点2-7)
	HBTC_addIdleCellToFreeList,//2
	HBTC_addReleasedCellToPreAllocList,//3
	HBTC_addPreAllocCellToReleaseList,//4
	//等待写值
	HBTC_WAIT_WRITE_VALUE,//5
	//等待确认
	HBTC_WAIT_WRITE_ACK, //6
	//确认使用分配的值  (事务完成点)
	HBTC_ACK_USED_VALUE, //7 
	//------------------------------------------------
	//删除值 (8 和9组成一个删除事务)
	HBTC_DELETE_VALUE, //8
	//删除成功
	HBTC_ACK_DELETE_VALUE//9
	//------------------------------------------------
};

struct HashBlockTransactionContext
{
	//第一次分配的单元编号
	volatile uint32_t  firstTimeAllocCellIndex;
	//事务状态
	volatile HashBlockTransactionStatus  status;
	
	//空闲cell list
	s_offset_list_t  freeCellList;
	//被分配的的cell list
	s_offset_list_t  allocedList;
	//当前操作的CELL
	HashValueCell*   m_curOpCell;
	HashValueCell*   m_curOpCell2;

	//------------------------------------------
	//当前操作的 valueSeq
	uint64_t valueSeq;
	//当前操作的valueLen
	uint64_t  valueLen;
	//--------------------------------------------
}__attribute__((__packed__));

struct HashBlockTail
{
	//块magicid
	char   magicID[16];// = { "_HashBlockTail_" };

}__attribute__((__packed__));


class HashValueBlock {
public:
	HashValueBlock();
	~HashValueBlock();
	//获取创建指定数量的cell需要的内存大小

	static  uint64_t GetCreateThisBlockNeedMemSize(const uint32_t cellNum, const uint32_t cellSize);
	static  bool     CheckCellDataSize(const uint32_t  cellDataSize);
	bool             Create(char* addr, uint64_t  blockSize, const uint32_t  cellNum, const uint32_t  cellDataSize);
public:
	//创建数据
	uint64_t   ReqCreateValue(uint8_t* value, uint32_t valueLen, uint64_t keySeq);
	//确认使用数据
	uint64_t   ReqAckUsedValue(uint64_t valueid, uint32_t valueLen, uint64_t keySeq);
	//读取value数据
	int        ReqReadValue(uint64_t valueID, uint64_t valueSeq, uint32_t valueLen, uint8_t* outValueP, uint32_t outBufSize);
	
	//删除指定valueID的value
	int        ReqDeleteValue(uint64_t id, uint64_t keySeq, uint32_t valueLen);
protected:
	void  initMemberAddress(char* addr, uint32_t  cellNum, uint32_t  cellDataSize);
	void  initHead(uint64_t  blockSize,uint32_t  cellNum, uint32_t  cellDataSize);
	bool  checkHead();
	void  initTranContext();
	bool  checkTranContext();
	void  initCellArray();
	bool  checkCellArray();
	void  initTail();
	bool  checkTail();
	void  initAll(uint64_t  blockSize, uint32_t  cellNum , uint32_t  cellDataSize);
	bool  checkAll();
public:
	inline  uint64_t CELL_PTR_TO_OFFSET(HashValueCell* p) { return  (uint64_t)((char*)p - GetBaseAddr());}
	HashValueCell*   CELL_OFFSET_TO_PTR(uint64_t offset);
	uint32_t         CELL_PTR_TO_INDEX(HashValueCell*);
	inline char*     GetBaseAddr() { return (char*)m_blockHead; }
	inline uint64_t  GetCellStartOffset() { return m_blockHead->cellStartOffset; }
	inline uint64_t  GetCellEndOffset() { return m_blockHead->cellEndOffset; }

protected:
	HashValueCell*   getCell(uint32_t cellIndex);
	uint64_t         getCellOffset(uint32_t cellIndex);

	void       setCellReleasedStatus(HashValueCell* cell);
	uint64_t   allocValueMemory(uint8_t* value, uint32_t valueLen, uint64_t keySeq);
	bool       writeValue(uint64_t offset,uint8_t* value, uint32_t valueLen, uint64_t keySeq);
	void addIdleCellToFreeList(int count);
	void addReleasedCellToPreAllocList(HashValueCell* cell, uint64_t valueSeq);
	void addPreAllocCellToReleaseList(HashValueCell* cell);
	void handleAddPreAllocCellToReleaseList();
	//处理当前事务
	void rollbackCurTransactionContext();
protected:
	void onStatusIdele();
	void onStatusAddIdleCellToFreeList();
	void onStatusAddPreAllocCellToReleaseList();
	void onStatusAddReleasedCellToPreAllocList();
	void onStatusWaitWriteValue();
	void onStatusWaitWriteAck();
	void onStatusAckUsedValue();
	void onStatusDeleteValue();
	void onStatusAckDeleteValue();
protected:
	//head(HashBlockHead)+(m_tranContext)HashBlockTransactionContext+m_cellArray(HashValueCell)+(m_blockTail)HashBlockTail
	HashBlockHead* m_blockHead;
	//块操作的事务信息
	HashBlockTransactionContext* m_tranContext;
	HashValueCell* m_cellArray;
	HashBlockTail* m_blockTail;

};

#endif
