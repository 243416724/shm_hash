#ifndef _MyMutex_H_
#define _MyMutex_H_

#include <queue>

#ifndef _WIN32
   #include <pthread.h> 
#else
   #include <Windows.h>
   #include <time.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> 


class  CMyMutex
{

public:
	CMyMutex();

	~CMyMutex();
	void lock();
	void unlock();
protected:

#ifdef WIN32
	CRITICAL_SECTION  m_mutex;
#else
	pthread_mutex_t   m_mutex;
	pthread_mutexattr_t attr;
#endif

};


class MyAutoLock
{
public:
	MyAutoLock(CMyMutex&  mutex,bool autoLock=true):
	  m_mutex(mutex),m_bAutoLock(autoLock)
	  {
		  if(m_bAutoLock)
		  {
			  m_mutex.lock();
		  }
	  }
	  ~MyAutoLock()
	  {
		  if(m_bAutoLock)
		  {
			  m_mutex.unlock();
		  }
	  }
protected:
	CMyMutex&  m_mutex;
	bool  m_bAutoLock;

};


typedef struct 
{
	//struct timeval start, end;
	unsigned  long  maxLockTime;

#ifdef WIN32
	CRITICAL_SECTION  m_mutex;
#else
	pthread_mutex_t   m_mutex;
#endif


}MyMutex;



void InitMyMutex(MyMutex* p);
void LockMyMutex(MyMutex* p);
void UnLockMyMutex(MyMutex* p);
void DestroyMyMutex(MyMutex* p);


typedef  struct 
{

#ifdef  _WIN32
		void*  m_hEvent;
		unsigned  char m_bManualReset;
		unsigned  char  m_bSignal;
#else
		pthread_mutex_t   m_mutex;
		pthread_cond_t    m_cond;
		unsigned  char           m_bSignal;//是否在信号状态
		unsigned  char           m_bManualReset;//手否手动重置信号状态
#endif

}MyEvent;


int  MyInitEvent(MyEvent*  pMyEvnet,
				 void*  lpEventAttributes,
				 unsigned  char  bManualReset,
				 unsigned  char  bInitialState,
				 const char*  lpName);
int  MyWaiteEvent(MyEvent*  pMyEvnet);
int  MyWaiteTimeOut(MyEvent*  pMyEvnet,unsigned int second,unsigned int millisecond);
int  MySetEvent(MyEvent*  pMyEvnet);
void MyResetEvent(MyEvent*  pMyEvnet);
void MyDestroyEvent(MyEvent*  pMyEvnet);


class MySimpleEventQueue
{
public:
	MySimpleEventQueue();
	~MySimpleEventQueue();
public:
	unsigned  char* GetMsg(unsigned int&  queueSize);
	void            PushMsg(unsigned  char* );
	int             Clear(int isDeleteMsg=0);
protected:
	MyMutex   m_mutex;
	MyEvent   m_event;
	std::queue<unsigned char*>  m_eventQueue;
};

struct  MySimpleMsg
{
	unsigned int      eventNo;
	unsigned short    msgLen;
	unsigned char*    msg;
};



typedef  int (*SimpleMsgEventQueuePtr)(unsigned  int evnet,unsigned int msgLen,unsigned char* pInData);

class  MySimpleMsgEventQueue
{
public:
	MySimpleMsgEventQueue();
	~MySimpleMsgEventQueue();
public:
	unsigned  int            GetMsg(MySimpleMsg*  pOutMsg);
	void            PushMsg(MySimpleMsg*  msg);
	void            PushMsg( unsigned int eventId, unsigned char* msg, unsigned int len);
public:
	void            SetSimpleMsgEventQueuePtr(SimpleMsgEventQueuePtr p){m_simpleMsgEventQueuePtr=p;}
	
protected:
	MyMutex   m_mutex;
	MyEvent   m_event;

	std::queue<MySimpleMsg>  m_eventQueue;

protected:
	SimpleMsgEventQueuePtr  m_simpleMsgEventQueuePtr;

	MyMutex                  m_backMutex;
	std::queue<MySimpleMsg>* m_pBackEventQueue;

	MyMutex                  m_frontMutex;
	std::queue<MySimpleMsg>* m_pFrontEventQueue;

	std::queue<MySimpleMsg>  m_eventQueue1;
	std::queue<MySimpleMsg>  m_eventQueue2;
protected:
	//当前处理事件
	unsigned  int            m_iCurHandleEvent;

};



#endif

