#include "shared_mutex.h"
#include <pthread.h>
#include "wx_log.h"
#include <errno.h>
//#define logerror  printf

SharedMutex::SharedMutex() {

}
SharedMutex::~SharedMutex() {

}

bool SharedMutex::isInited() {
	loginfo("m_mutexBeginTag=%s", m_mutexBeginTag);
	loginfo("m_mutexEndTag=%s", m_mutexEndTag);
	if (strcmp(m_mutexBeginTag, "_mutex_begin_") != 0) {
		
		return false;
	}
	if (strcmp(m_mutexEndTag, "_mutex_end_") != 0) {
		return false;
	}

	int robust = 0;
	pthread_mutexattr_getrobust(&m_attr, &robust);
	if (robust != PTHREAD_MUTEX_ROBUST)
	{
		logerror("robust != PTHREAD_MUTEX_ROBUST\n");
		return  false;
	}
	else {
		logdebug("PTHREAD_MUTEX_ROBUST[true]");
	}
	int shared = 0;
	pthread_mutexattr_getpshared(&m_attr, &shared);
	if (shared != PTHREAD_PROCESS_SHARED)
	{
		logerror("shared != PTHREAD_PROCESS_SHARED\n");
		return  false;
	}
	else {
		logdebug("PTHREAD_PROCESS_SHARED[true]");
	}
	//printf("pthread_mutexattr_getpshared return\n");
	return true;
}
bool SharedMutex::init() {

	logwarn("SharedMutex init begin");
	strcpy(m_mutexBeginTag, "_mutex_begin_");

	pthread_mutexattr_init(&m_attr);
	if (0 != pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_RECURSIVE))
	{
		logerror("pthread_mutexattr_settype PTHREAD_MUTEX_RECURSIVE  failed\n");
		return  false;
	}

	if (0 != pthread_mutexattr_setpshared(&m_attr, PTHREAD_PROCESS_SHARED))
	{
		//设置互斥对象为PTHREAD_PROCESS_SHARED共享，即可以在多个进程的线程访问,PTHREAD_PROCESS_PRIVATE为同一进程的线程共享   
		logerror("pthread_mutexattr_setpshared  failed\n");
		return false;

	}
	if (0 != pthread_mutexattr_setrobust(&m_attr, PTHREAD_MUTEX_ROBUST))
	{
		//设置锁要有健壮性
		logerror("pthread_mutexattr_setrobust failed\n");
		return  false;
	}

	if (0 != pthread_mutex_init(&m_mutex, &m_attr))
	{
		logerror("pthread_mutex_init  failed\n");
		return false;
	}

	strcpy(m_mutexEndTag, "_mutex_end_");
	loginfo("SharedMutex end init");
	return true;
}

int  SharedMutex::create(bool new_flag)
{   
	if (!isInited()) {
		loginfo("SharedMutex isInited[false]");
		init();
		isInited();
	}
	return 0;

#if  0
	printf("SharedMutex size=[%d]\n",sizeof(SharedMutex));
	unsigned  long object = (unsigned  long)(void*)this;
	if (new_flag)
	{

		printf("need init\n");
		//m_flag = object;

		pthread_mutexattr_init(&m_attr);

		if (0 != pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_RECURSIVE))
		{
			logerror("pthread_mutexattr_settype PTHREAD_MUTEX_RECURSIVE  failed\n");
			return  -1;
		}

		if (0 != pthread_mutexattr_setpshared(&m_attr, PTHREAD_PROCESS_SHARED))
		{
			//设置互斥对象为PTHREAD_PROCESS_SHARED共享，即可以在多个进程的线程访问,PTHREAD_PROCESS_PRIVATE为同一进程的线程共享   
			logerror("pthread_mutexattr_setpshared  failed\n");
			return 1;

		}
		if (0 != pthread_mutexattr_setrobust(&m_attr, PTHREAD_MUTEX_ROBUST)) 
		{
			//设置锁要有健壮性
			logerror("pthread_mutexattr_setrobust failed\n");
			return  2;
		}

		if (0 != pthread_mutex_init(&m_mutex, &m_attr))
		{
			logerror("pthread_mutex_init  failed\n");
			return 3;
		}
		return  0;
	}
	else
	{
		//check 
		/*
		if (m_flag != object)
		{
			printf("flag error\n");
			return  4;
		}
		*/
		printf("not  need init,i  check\n");
		int robust = 0;
		pthread_mutexattr_getrobust(&m_attr,&robust);
		if (robust != PTHREAD_MUTEX_ROBUST)
		{
			logerror("robust != PTHREAD_MUTEX_ROBUST\n");
			return  5;
		}
		int shared = 0;
		pthread_mutexattr_getpshared(&m_attr,&shared);
		if (shared != PTHREAD_PROCESS_SHARED)
		{
			logerror("shared != PTHREAD_PROCESS_SHARED\n");
			return  5;
		}
		printf("pthread_mutexattr_getpshared return\n");
		return 0;

	}
#endif


}
bool SharedMutex::lock()
{
	//printf("begin wait mutex_lock...\n");
	int  errorEOWNERDEAD = 0;
	while (1)
	{
		logwarn("begin pthread_mutex_lock");
		int ret = pthread_mutex_lock(&m_mutex);
		logwarn("pthread_mutex_lock ret=%d", ret);
		if (ret == 0) {
			break;
		}

		if (ret == EOWNERDEAD)
		{
			printf("pthread_mutex_lock  failed ,EOWNERDEAD\n");
			logwarn("pthread_mutex_lock  failed ,EOWNERDEAD");
			if (0 != pthread_mutex_consistent(&m_mutex)) {
				printf("pthread_mutex_consistent  failed \n");
			}
			if (0 != pthread_mutex_unlock(&m_mutex)) {
				printf("pthread_mutex_unlock  failed \n");
			}
			errorEOWNERDEAD = 1;
		}
		else
		{
			printf("lock failed ,other error\n");
		}
	}
	//printf("mutex_lock sucess\n");
	return (errorEOWNERDEAD == 0);
}

void SharedMutex::unlock()
{
	//printf("mutex_unlock begin\n");
	pthread_mutex_unlock(&m_mutex);
	//printf("mutex_unlock sucess\n");
}




void outError() {

	switch (errno) {
	case EACCES:
		logerror("EACCES");
		break;
	case EEXIST:
		logerror("EEXIST");
		break;
	case EINVAL:
		logerror("EINVAL");
		break;
	case ENFILE:
		logerror("ENFILE");
		break;
	case ENOENT:
		logerror("ENOENT");
		break;
	case ENOMEM:
		logerror("ENOMEM");
		break;
	default:
		logerror("other");

	}
}

void *getmemory(int shmkey, size_t  shmsize, int &new_falag)
{
	size_t oldshmsize = shmsize;
	size_t numpage = shmsize / 4096;
	size_t ceil = shmsize % 4096;
	if (ceil !=0) {
		numpage += 1;	
	}
	shmsize = 4096 * numpage;
	logwarn("getmemory change shmsize:%ld,shmsize to:%ld", oldshmsize, shmsize);



	int32_t shmid;
	void *shmmem;
	new_falag = 0;

	if ((shmid = shmget(shmkey, shmsize, 0)) != -1)
	{
		shmmem = shmat(shmid, NULL, 0);

		if (shmmem != MAP_FAILED)
		{
			//LogDebug("attach a exist shm mem");
			new_falag = 0;
			return shmmem;
		}

		fprintf(stderr, "Shm map failed:shmkey=%x,shmsize=%ld.ErrMsg:%m\n", shmkey, shmsize);
		return NULL;
	}
	else
	{
		shmid = shmget(shmkey, shmsize, IPC_CREAT | 0666);
		

		if (shmid != -1)
		{
			shmmem = shmat(shmid, NULL, 0);

			if (shmmem != MAP_FAILED)
			{
				LogInfo(" new create  a shm mem:%p", shmmem);
				new_falag = 1;
				return shmmem;
			}
			else
			{
				fprintf(stderr, "Shm map failed:shmkey=%x,shmsize=%ld.ErrMsg:%m\n", shmkey, shmsize);
				return NULL;
			}
		}
		else
		{
			outError();
			fprintf(stderr, "Shm create failed:shmkey=%x,shmsize=%ld.ErrMsg:%m\n", shmkey, shmsize);
			return NULL;
		}
	}

	return NULL;
}




/**

int main(int argc,char** argv)
{
	int key = 0x1421983;


	int new_flag;
	void*  p = getmemory(key, 2048, new_flag);
	if (p == NULL) {
		printf("getmemory  failed\n");
		return  0;
	}

	SharedMutex*  mutex = (SharedMutex*)p;
	mutex->create(new_flag);

	char* pint = (char*)p + sizeof(SharedMutex);

	
	int num = 0;
	while(num<5)
	{
		mutex->lock();
		
		getchar();

		num += 1;

		printf("i acquire lock count[%d]\n", num);
		mutex->unlock();
		getchar();

		
	}
	mutex->lock();


	return  0;
}
*/