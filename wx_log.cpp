#include "wx_log.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include "my_mutex.h"

static const char*  
LogLevelInfo[]=
{	
	"[DEBUG]",
	"[TRACE]",
	"[INFO ]",
	"[WARN ]",
	"[ERROR]",
	"[FATAL]"
};



char COLOR_DEF[][32] =
{
	"\033[00;32m",//GREEN  DEBUG
	"\033[00;35m",//BLUE TRACE
	"\033[01;36m",//CYAN INFO
	"\033[01;33m",//BOLD YELLOW WARN
	"\033[01;31m", // # BOLD RED
	"\033[00;31m"  //# BOLD RED
};

#if  0
class ColorHandler(logging.StreamHandler) :
	LEVEL_COLORS = {
	logging.DEBUG: '\033[00;32m',  # GREEN
	logging.INFO: '\033[00;36m',  # CYAN
	logging.AUDIT: '\033[01;36m',  # BOLD CYAN
	logging.WARN: '\033[01;33m',  # BOLD YELLOW
	logging.ERROR: '\033[01;31m',  # BOLD RED
	logging.CRITICAL: '\033[01;31m',  # BOLD RED
}
#endif


#define  CLOSE_COLOR "\033[0m"



static int         g_logLevel=0; 
static bool        g_enableLogToConsole=true;
static bool        g_enableLogToFile = true;


static unsigned int   g_lMaxFileSize=5*1024*1024;
static char           g_strLogFilePath[1500]={0};

static  MyMutex*  plogMutex=NULL;

void  wx_set_log_level(int level)
{
	g_logLevel=level;
}

int    wx_get_log_level()
{
	return g_logLevel;
}
void wx_log_init(int level, bool log_to_concole, bool log_to_file)
{
	if(plogMutex)
	{
		return;
	}

	plogMutex=(MyMutex*)malloc(sizeof(MyMutex));
	memset((void*)plogMutex,0,sizeof(MyMutex));
	InitMyMutex(plogMutex);

	g_logLevel = level;
	g_enableLogToConsole = log_to_concole;
	g_enableLogToFile = log_to_file;
}


void wx_set_logfilepath(const char* path)
{
	strcpy(g_strLogFilePath,path);
}

void wx_set_logtoconsole(int tag)
{
	g_enableLogToConsole = tag;
}


	
static  inline  struct tm*
wx_log_get_cur_time()
{
	struct tm* presult = NULL;
	time_t nowtime = time(NULL);
#ifndef _WIN32
	static  struct tm   result = { 0 };
	localtime_r(&nowtime, &result);
	presult = &result;
#else
	presult = localtime(&nowtime);
#endif

	return presult;
}

static  inline   
void wx_log_get_trim_path(char ** filePath)
{

	 char* codeFileName=*filePath;
#ifdef WIN32
	//去掉文件名中的路径(代码文件)
	const char*  ppos=strrchr(codeFileName,'\\');
	if(ppos)
	{
		*filePath=ppos+1;
	}
#else
	//去掉文件名中的路径(代码文件)
	char*  ppos=strrchr(codeFileName,'/');
	if(ppos)
	{
		*filePath=ppos+1;
	}
#endif
}

static  inline   
void  wx_log_get_log_file(char*  newLogFileName,char* destLogFileName,struct tm* presult)
{

	if(g_strLogFilePath[0]!=0)
	{

		sprintf(newLogFileName,"%s/%s_%02d%02d%02d%02d%02d.log",

			g_strLogFilePath,				
			destLogFileName,

			  (presult->tm_mon+1),
				presult->tm_mday,
				presult->tm_hour,
				presult->tm_min,
				presult->tm_sec
			);
	}
	else
	{
		sprintf(newLogFileName,"./log/%s_%02d%02d%02d%02d%02d.log",


			destLogFileName,
			  (presult->tm_mon+1),
				presult->tm_mday,
				presult->tm_hour,
				presult->tm_min,
				presult->tm_sec
			);

	}

}

static  inline  
void wx_log_to_file(struct tm* presult,char*  logmsg,char* destLogFileName,char*  log_file_path)
{
	
	unsigned  int filesize=0;
	 FILE*  file=fopen(log_file_path,"a");
	 if(file==NULL){
	     printf("Failed to open  log file %s\n", log_file_path);
		 return ;
	 }

	 filesize= ftell(file);
            
	 if((filesize>g_lMaxFileSize)&&(filesize!=0xFFFFFFFF))
	 {
			//超过了指定的文件大小,将当前文件重命名
			char syscmd[1024]={0};
			char newLogFileName[1024]={0};
			fclose(file);
			file=NULL;

			wx_log_get_log_file(newLogFileName,destLogFileName,presult);														
			#ifndef _WIN32
				sprintf(syscmd,"mv  %s  %s",log_file_path,newLogFileName);
				system(syscmd);
			#endif
			file=fopen(log_file_path,"a");
			if(file==NULL){
				return ;
			}
		
	}

	fprintf(file,"%s",logmsg);
	fclose(file);
}

void wx_log(int loglevel,int forceLogOut,const  char* codeFileName1,const char* funcname,int line,const  char* logFileName1,const char* prompt,...)
{
	if((loglevel<0)||(loglevel>6)){
	  return ;
	}

	if(g_logLevel>loglevel){
		return;
	}
	
	LockMyMutex(plogMutex);

	struct tm* presult=wx_log_get_cur_time();
	char* codeFileName=(char*)codeFileName1;
	wx_log_get_trim_path(&codeFileName);
	
	char  destLogFileName[1024]={0};
	char* logFileName=(char*)logFileName1;
	if(logFileName){
		wx_log_get_trim_path(&logFileName);
		strcpy(destLogFileName,logFileName);
	}

	//去掉文件名中的扩展名
	char*  ppos2=strrchr(destLogFileName,'.');
	if(ppos2){
		*ppos2='\0';
	}

	char  logmsg[8192]={0};
	int   logPos=0;

	logPos+=sprintf(logmsg+ logPos,"%s %02u-%02u %02d:%02d:%02d   ",LogLevelInfo[loglevel] ,
																		(presult->tm_mon+1),
																		presult->tm_mday,
																		presult->tm_hour,
																		presult->tm_min,
																		presult->tm_sec);

	logPos += sprintf(logmsg + logPos, "F[%30.30s] U[%30.30s] L[%5d] ", codeFileName, funcname, line);

	/*输出日志内容*/
	 va_list  args1;
	 va_start( args1, prompt);
	 logPos+=vsnprintf(logmsg +logPos, (8190-logPos),  prompt, args1);
	 va_end(args1);

	

	 if (logmsg[logPos - 1] != '\n') {
		 /*增加个换行符*/
		 logmsg[logPos] = '\n';
		 logPos += 1;
	 }

	 char fileName[1500]={0};
	 if(g_strLogFilePath[0]!=0)
	 {
		 sprintf(fileName,   "%s/%s_%02d%02d.log",g_strLogFilePath,destLogFileName,(presult->tm_mon+1),presult->tm_mday);
	 }
	 else
	 {
		 sprintf(fileName,   "./log/%s_%02d%02d.log",                 destLogFileName,(presult->tm_mon+1),presult->tm_mday);
	 }

	 /*是否需要输出到控制台*/
	 if(g_enableLogToConsole)
	 {
		  fprintf(stderr,"%s%s%s", COLOR_DEF[loglevel],logmsg, CLOSE_COLOR);
	 }

	  wx_log_to_file(presult,logmsg,destLogFileName,fileName);

	  UnLockMyMutex(plogMutex);

}

