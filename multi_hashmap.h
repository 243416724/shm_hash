#ifndef _multi_hashmap_h_
#define _multi_hashmap_h_


#include "define.h"
#include "hashvalue_block.h"
#include <stdint.h>
#include <vector>
#include <string>
#include "shared_mutex.h"
#include "simple_offset_list.h"

struct MhtHeader {
	//块magicid
	char   magicID[16];// = { "_MhtHead_" };
	//版本号
	uint16_t version;
	//大小,包括头大小
	uint32_t  size;
	//头大小
	uint16_t headerSzie;
	//总行数
	uint16_t nodesRowNum;
	//总节点数
	uint32_t totalNodesNum;
	//key的size
	uint8_t keyDataSize;
	uint16_t nodeSize;
	char    blockInitTag[16];
}__attribute__((__packed__));

enum MhtNodeStatus {
	MNS_INVALID,
	//这个节点准备被删除
	MNS_PRE_DELETED,
	//被分配准备使用,
	MNS_PRE_ALLOCED,
	//操作成功
	MNS_SETED_OK,
	//进行替换值
	MNS_REPLACE_VAL	
};

struct MhtHashNode {
	volatile uint64_t next;//8
	//节点状态 MhtNodeStatus
	volatile uint8_t  keyStatus;
	uint8_t  keyDataLen;
	//节点数据类型
	uint8_t  keyDataType;
	uint32_t valueLen;
	//操作开始时间
	uint32_t opStartTime;
	uint64_t valueID;
	//值变化的sql,全局
	uint64_t valueSeq;

	uint64_t preAllocValueIDSeq;
	uint64_t preAllocNewValueID;
	uint32_t preAllocNewValueLen;
	//这个节点自己变化的sq
	uint64_t nodeSeq;
	uint8_t  keyData[HASH_NODE_KEY_DATA_SIZE];
}__attribute__((__packed__));

struct MhtContext {
	//操作的keysql序号
	uint64_t keySeq;
	s_offset_list_t   inOpList;

}__attribute__((__packed__));

struct MhtRowHeader {
	char  rowTag[8];//="_ROW_"
	//本行节点总数
	uint32_t  nodeNum;
	//占用的大小
	uint32_t  rowBodySize;

}__attribute__((__packed__));

struct MhtTail {
	//块magicid
	char   magicID[16];// = { "_MhtTail_" };
}__attribute__((__packed__));

class MultiHashMap {
public:
	MultiHashMap();
	~MultiHashMap();
public:
	
	static  std::vector<int> GetCreateThisBlockNeedMemSize(const uint32_t inHashFirstRowsNodeNum, const uint32_t inHashRowsNum, 
		uint64_t& outKeyMemTotalSize, uint64_t& outValueMemTotalSize, uint64_t& outCellNum, 
		uint32_t valueDataSize);

	bool  Create(int key, const uint32_t inHashFirstRowsNodeNum, const uint32_t valueDataSize= HashBlockDefaultCellDataSize, uint32_t inHashRowsNum=HashNodeRowsDefaultNum);
	uint32_t GetTotalNodesNum() { return m_header->totalNodesNum; }
	void Info() { printAll(); }

protected:
	static  std::vector<int> getCreateThisBlockHashRowNodesInfo(const uint32_t inHashFirstRowsNodeNum, const uint32_t inHashRowsNum);
	static  void calCreateThisBlockNeedMemSize(const std::vector<int>& nodesInfo, uint64_t& outKeyMemTotalSize, uint64_t& outValueMemTotalSize, uint64_t&  outCellNum,uint32_t& cellSize);
	void  initMemberAddress(const char* addr, const std::vector<int>& nodesInfo, bool keyMemIsNewCreate);
	void  initHead(uint64_t  blockSize);
	bool  checkHead();
	void  printHead();
	void  initContext();
	bool  checkContext();
	void  printContext();
	void  initRowLines();
	bool  checkRowLines();
	void  printRowLines();
	void  initTail();
	bool  checkTail();
	void  printTail();
	void  initAll(uint64_t  blockSize);
	bool  checkAll();
	void  printAll();

public:
	//从共享内存上创建
	void CreateFromShm();
	//查询获取key的值
	bool GetKey(uint8_t* key, uint8_t keyLen, uint8_t** outValueP, uint32_t& outValueLen);
	//查找key,返回读取到的数据长度，没查找到返回0,outValueLen 是实际的长度
	int  GetKeyWithBuf(uint8_t* key, uint8_t keyLen,  uint8_t* inBuf, uint32_t  bufSize, uint32_t& outValueLen);

	std::string GetKeyStringValue(const char* key);
	const char* GetKeyCharValue(const char* key);

	int  FreeGetKeyMem(uint8_t* p);
	bool HasKey(uint8_t* key, uint8_t keyLen);

	int  SetKey(const char* key, const char* val);
	int  SetKey(uint8_t* key, uint8_t keyLen, uint8_t* value, uint32_t valueLen);
	//不存在才设置值(先不支持)
	//int  SetKeyIfNotExisted(uint8_t* key, uint8_t keyLen, uint8_t* value, uint32_t valueLen);
	int  DeleteKey(uint8_t* key, uint8_t keyLen);
	uint8_t* GetBaseAddr();
protected:
	void lock();
	void unlock();
	void ReadDataFromNode(MhtHashNode*  from,bool needReadValue, MhtHashNode* outNode, uint8_t** outValue);
	int  ReadDataFromNodeWithBuf(MhtHashNode* from, bool needReadValue, MhtHashNode* outNode,uint8_t*  inBuf, uint32_t inBufSize);
	
	//查找key
	bool findKey(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtHashNode* outNode,uint8_t** outValue);

	MhtHashNode* getSameKeyFromRowForSet(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtRowHeader* row);
	MhtHashNode* getSameKeyFromRowForInsert(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtRowHeader* row);
	MhtHashNode* getSameKeyForSet(uint8_t* key, uint8_t keyLen, uint64_t keyID);
	MhtHashNode* getSameKeyForInsert(uint8_t* key, uint8_t keyLen, uint64_t keyID);

	bool getKeyFromRow       (uint8_t* key, uint8_t keyLen, uint64_t keyID ,MhtRowHeader* row, MhtHashNode* outNode, uint8_t** outValue);
	bool getKeyFromRowWithBuf(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtRowHeader* row, MhtHashNode* outNode, uint8_t*      inBuf, uint32_t inBufSize);
	bool hasKeyFromRow(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtRowHeader* row, MhtHashNode* outNode);
	MhtHashNode* getBlankKeyNodeForWrite(uint8_t* key, uint8_t keyLen, uint64_t keyID);
	MhtHashNode* getBlankKeyNodeFromRowForWrite(uint8_t* key, uint8_t keyLen, uint64_t keyID, MhtRowHeader* row);

	//修复异常
	void repareException();
	void handleExceptionNode(MhtHashNode* node, s_offset_list_t* pre_node);
	bool  nodeIsEndStatus(MhtHashNode* node);

protected:
	MhtHeader       *m_header=nullptr;
	MhtContext      *m_context=nullptr;
	MhtTail         *m_htTail=nullptr;

	std::vector<MhtRowHeader*>  m_rowLines;
	std::vector<int>  m_rowsPrimNumInfo;
protected:
	HashValueBlock     m_hashValueBlock;
	SharedMutex       *m_mutex=nullptr;

	int                m_keyMemKey = 0;
	int                m_valMemKey = 0;
	int                m_mutexKey=0;
	
};

#endif
